const { Transform } = require('stream');
const fs = require('fs');

/*
 * main function
 * This function takes a buffer as a stream and returns a stream of reports describing the number of lines processed/chunk and at a rate of bytes/sec
 * @param {array} processargs; the expected input is the process.argv array which is array of the user inputs in command line, e.g. ["/source/path/node", "/path/to/index.js", "--verbose"]
 * @return {stream} returns a stream of strings reporting on chunk summaries; streams to standard output or file depending on arguments
 */
function fileStreamReporter(processargs) {


    const args = getArgvs(processargs);
    let verbose = false;
    let file;
    let outFile;


    for (i = 0; i < processargs.length; i++) {

        if (args[i] === "--file") {
            file = args[i+1];
        };
        if (args[i] === "--verbose") {
            verbose = true;
            count = 0;
        };
        if (args[i] === "--out") {
            outFile = args[i+1];
        };

    };


    if (file) {

        if (!fs.existsSync(file)) {
            console.log(`${ file } does not exist. Use a different path.`);
            return;
        }

        const stats = fs.statSync(file);
        const fileSizeInBytes = stats.size;
        if (fileSizeInBytes === 0) {
            console.log("file is empty.");
            return;
        };

    };


    if (outFile) {

        if (fs.existsSync(outFile)) {
            console.log(`${ outFile } exists. Use a different path.`);
            return;
        };

        const out = fs.createWriteStream(`${outFile}`);
        if (file) {
            const streamFile = fs.createReadStream(`${file}`);

            injectThroughStreams(streamFile, out);
            return;
        };

        injectThroughStreams(process.stdin, out);
        return;

    }


    if (file) {
        const streamFile = fs.createReadStream(`${file}`);

        injectThroughStreams(streamFile, process.stdout);
        return;

    }


    injectThroughStreams(process.stdin, process.stdout);
    return;


    /*
     * returns an infrastructure of pipes that takes an input stream and transforms the buffer into a stream of strings of human readable reports that is then passed onto output stream; core functionality of filestreamreporter
     * @param {stream} inStream; the writable stream source; only takes in the default stream types, e.g. buffer, string, etc.
     * @param {stream} outStream; the output readable stream that takes in a stream of strings
     * @return {streams}
     */

    function injectThroughStreams(inStream, outStream) {

        const throughStreams = (
            inStream
                .on('error', function(error) { handleAnyError(error) })
                .pipe(chunkToObjectStream(verbose))
                .on('error', function(error) { handleAnyError(error) })
                .pipe(objectToStringStream())
                .on('error', function(error) { handleAnyError(error) })
                .pipe(outStream)
        )

        return throughStreams;

    };


};

/*
 * returns an object containing keys as indexes and args as values
 * @param {array} args; primarily used to take in the process.argv array
 * @return {object}
 */
function getArgvs(args) {

    const argvs = {};

    args.forEach((val, index) => {
        argvs[index] = val;
    })

    return argvs;

}

/*
 * returns a writable stream stream of object containing keys for bytes, lines, duration
 * @param {boolean} verbose; true to display intermediary summaries in console as chunks are streamed
 * @return {stream}
 */
function chunkToObjectStream(verbose) {

    let count;
    if (verbose) {
        count = 0;
    }

    const chunkToObject = new Transform({

        readableObjectMode: true,

        transform(chunk, encoding, callback) {

            if (verbose) {
                count += 1;
                let message = `Breaking down chunk ${ count } ...\n`;
                process.stdout.write(message);
            };

            const obj = chunkObject(chunk)

            if (verbose) {
                let messageJson = JSON.stringify(obj);
                let message = `Raw stats of chunk ${ count }:\n ${messageJson}\n`;
                process.stdout.write(message);
            }

            this.push(obj);
            callback();
        },

    });

    return chunkToObject;

};

/*
 * returns an object with summaries of the input buffer including bytes, lines, duration
 * @param {buffer} chunk; an input stream of data primarily from a writable stream source
 * @return {object}
 */
function chunkObject(chunk) {

    const start = process.hrtime();
    const totalBytes = chunk.length;
    const totalLines = chunk.toString().split('\n').length;
    const end = process.hrtime(start);


    const NS_PER_SEC = 1e9;
    const totalSeconds = end[0] + end[1]/NS_PER_SEC;


    const obj = {
        "bytes": totalBytes,
        "lines": totalLines,
        "duration": totalSeconds,
    };


    return obj;
};

/**
 * through stream that requires a readable stream of objects containing keys for bytes, lines, durationa and returns a stream of strings containing human readable report
 * @return {stream}
 */
function objectToStringStream() {

    const objectToString = new Transform({

        writableObjectMode: true,

        transform(chunk, encoding, callback) {

            const report = objectReport(chunk.bytes, chunk.duration, chunk.lines);
            this.push(report);
            callback();

        },

    });

    return objectToString;

};

/*
 * returns a human readable string that summarises a given byte size, duration, and number of lines
 * @param {number} bytes; referred to as the length or size of the chunk processed
 * @param {number} duration; referred to the time elapsed to process the chunk summaries
 * @param {number} lines; referred to the number of lines processed in the chunk
 * @return {string}
 */
function objectReport(bytes, duration, lines) {

    const processRate = bytes / duration;
    const report = `${ lines } lines processed at a rate of ${ processRate } bytes/sec\n`;
    return report;

};

/*
 * console logs errors and stops process
 * @param {error} error; any error ocurred during streaming
 */
 function handleAnyError(error) {

    console.log(error);
    return;

 };


module.exports = {
    getArgvs,
    chunkToObjectStream,
    objectToStringStream,
    fileStreamReporter,
    objectReport,
    chunkObject,
};