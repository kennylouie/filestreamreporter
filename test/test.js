const expect = require('chai').expect;
const { Readable } = require('stream');
const fs = require('fs');

const streamsUtils = require('../lib/streams/streams.utils');

describe('#streamsUtils.getArgvs', function(){
    it('should convert array (usually for process.argv inputs) to object with keys as indexes', function() {
        const testObj = ['node', 'index.js', '--verbose'];
        const actual = streamsUtils.getArgvs(testObj);
        const expected = {
            '0': 'node',
            '1': 'index.js',
            '2': '--verbose',
        };
        expect(actual).to.deep.equal(expected);
    });
    it('should convert empty array to empty object', function() {
        const testObj = [];
        const actual = streamsUtils.getArgvs(testObj);
        const expected = {};
        expect(actual).to.deep.equal(expected);
    });
});

describe('#streamsUtils.fileStreamReporter', function(){
    it('should take in an input stream of lines from a text file and output a reporting line to a text file with arguments --file and --out', function(){
        const testBuffer = "test";
        const testFile = fs.writeFileSync('./test.file', testBuffer);

        const args = ["--file", "test.file", "--out", "result.file"];
        streamsUtils.fileStreamReporter(args);

        fs.readFile('./result.file', (err, data) => {
            if (err) throw err;
            const actual = data.toString();
            expect(actual.charAt(0)).to.equal('1');
        });

        fs.unlink('./test.file', (err) => {
          if (err) throw err;
        });

        fs.unlink('./result.file', (err) => {
          if (err) throw err;
        });

    });

});

describe('#streamsUtils.chunkToObjectStream', function(){
    it('should take in an input stream of lines and return a stream of objects', function(){
        const testBuffer = "test"

        const fakeStream = new Readable({
            objectMode: true,
            read() {
                this.push(testBuffer);
                this.push(null);
            },
        });

        const expected = {
            "bytes": 4,
            "lines": 1,
        }
        var actual;
        var verbose = false;

        fakeStream
            .pipe(streamsUtils.chunkToObjectStream(verbose))
            .on('data', function(chunk) {
                actual = chunk;
            })
            .on('end', function() {
                expect(actual.bytes).to.equal(expected.bytes);
                expect(actual.lines).to.equal(expected.lines);
                expect(typeof actual.duration).to.equal("number");
            })
    });
});

describe('#streamsUtils.chunkObject', function(){
    it('should take in a buffer returning an object with summaries', function(){
        const testBuffer = "test"
        const actual = streamsUtils.chunkObject(testBuffer);
        const expected = {
            "bytes": 4,
            "lines": 1,
        }
        expect(actual.bytes).to.equal(expected.bytes);
        expect(actual.lines).to.equal(expected.lines);
        expect(typeof actual.duration).to.equal("number");
    });
});


describe('#streamsUtils.objectToStringStream', function(){
    it('should take in an input stream of objects summarizing lines, bytes, and duration and returns a string stream of those summaries', function(){
        const testBytes = 1;
        const testDuration = 1;
        const testLines = 1;
        const testObj = {
            "bytes": testBytes,
            "lines": testLines,
            "duration": testDuration,
        };

        const fakeStream = new Readable({
            objectMode: true,
            read() {
                this.push(testObj);
                this.push(null);
            },
        });

        var actual;

        fakeStream
            .pipe(streamsUtils.objectToStringStream())
            .on('data', function(chunk) {
                actual = chunk.toString();
            })
            .on('end', function() {
                var expected = `${ testLines } lines processed at a rate of ${ testBytes / testDuration } bytes/sec\n`
                expect(actual).to.equal(expected);
            })
    });
});

describe('#streamsUtils.objectReport', function(){
    it('should take in an lines, bytes, and duration and outputs a single liner report', function(){
        const testBytes = 1;
        const testDuration = 1;
        const testLines = 1;

        const actual = streamsUtils.objectReport(testBytes, testDuration, testLines);
        const expected = `${ testLines } lines processed at a rate of ${ testBytes / testDuration } bytes/sec\n`
        expect(actual).to.equal(expected);
    });
});

