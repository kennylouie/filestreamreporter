File Stream Reporter
===

A small javascript command line tool that parses line separated text files into a stream and outputs a summary of the lines

Example usage:

```
cat README.md | fsr --verbose
```

output:

```
85 lines processed at a rate of 20570695.984926477 bytes/s
```

## Installation

```
git clone https://gitlab.com/kennylouie/filestreamreporter.git
cd filestreamreporter
```

## To use the tool without installing

```
npm link
```

To unlink:

```
npm unlink
```

## Usage

To use live on terminal as stdin:

```
fsr
```

To use on files:

```
fsr --file file
```

Can be used with piping in:

```
cat file.text | fsr
```

If not using npm link, the index file can be used as a script file:

```
node index.js --file file
```

## Arguments

To show verbose terminal output of the process:

```
fsr --file README.md --verbose
```
Output:

```
Breaking down chunk 1 ...
Raw stats of chunk 1:
 {"bytes":1059,"lines":85,"duration":0.000051481}
85 lines processed at a rate of 20570695.984926477 bytes/s
```

The final report lines can be outputted to a destination file:

```
fsr --file file --out outputfile
```

## Tests

To run the tests:

```
npm install
npm test
```