const streamsUtils = require('./lib/streams/streams.utils');

/*
 * main function
 * This function takes a buffer as a stream and returns a stream of reports describing the number of lines processed/chunk and at a rate of bytes/sec
 * @param {array} processargs
 * @return {stream}
 */
streamsUtils.fileStreamReporter(process.argv);